package com.fendonus.mvvmwithretrofitandpaging.model;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.DataSource;

import com.fendonus.mvvmwithretrofitandpaging.network.GetDataService;

import java.util.List;

public class PhotoDataSourceFactory extends DataSource.Factory {

    PhotoDataSource photoDataSource;
    MutableLiveData<PhotoDataSource> mutableLiveData;

    public PhotoDataSourceFactory() {
        mutableLiveData = new MutableLiveData<>();
    }

    @Override
    public DataSource create() {
        photoDataSource = new PhotoDataSource();
        mutableLiveData.postValue(photoDataSource);
        return photoDataSource;
    }

    public MutableLiveData<PhotoDataSource> getMutableLiveData() {
        return mutableLiveData;
    }
}
