package com.fendonus.mvvmwithretrofitandpaging.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.support.annotation.NonNull;

import com.fendonus.mvvmwithretrofitandpaging.model.PhotoDataSource;
import com.fendonus.mvvmwithretrofitandpaging.model.PhotoDataSourceFactory;
import com.fendonus.mvvmwithretrofitandpaging.model.PhotoRepository;
import com.fendonus.mvvmwithretrofitandpaging.model.Photos;
import com.fendonus.mvvmwithretrofitandpaging.network.GetDataService;
import com.fendonus.mvvmwithretrofitandpaging.network.RetrofitClientInstance;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class MainActivityViewModel extends AndroidViewModel {

    PhotoRepository photoRepository;
    PhotoDataSourceFactory photoDataSourceFactory;
    MutableLiveData<PhotoDataSource> dataSourceMutableLiveData;
    Executor executor;
    LiveData<PagedList<Photos>> pagedListLiveData;

    public MainActivityViewModel(@NonNull Application application) {
        super(application);
        photoDataSourceFactory = new PhotoDataSourceFactory();
        dataSourceMutableLiveData = photoDataSourceFactory.getMutableLiveData();

        PagedList.Config config = (new PagedList.Config.Builder())
                .setEnablePlaceholders(true)
                .setInitialLoadSizeHint(10)
                .setPageSize(20)
                .setPrefetchDistance(4)
                .build();
        executor = Executors.newFixedThreadPool(5);
        pagedListLiveData = (new LivePagedListBuilder<Long,Photos>(photoDataSourceFactory,config))
                .setFetchExecutor(executor)
                .build();


    }

    public LiveData<PagedList<Photos>> getPagedListLiveData() {
        return pagedListLiveData;
    }
}
