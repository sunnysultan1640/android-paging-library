package com.fendonus.mvvmwithretrofitandpaging.adapter;


import android.arch.lifecycle.LiveData;
import android.arch.paging.PagedListAdapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.fendonus.mvvmwithretrofitandpaging.R;
import com.fendonus.mvvmwithretrofitandpaging.model.Photos;

import java.util.List;

public class PhotosAdapter extends PagedListAdapter<Photos,PhotosAdapter.PhotoViewHolder> {



    public PhotosAdapter() {
        super(Photos.CALLBACK);
    }

    @NonNull
    @Override
    public PhotoViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.list_photoes,viewGroup,false);
        return new PhotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PhotoViewHolder photoViewHolder, int i) {

        Glide.with(photoViewHolder.itemView.getContext()).load(getItem(i).getUrl()).into(photoViewHolder.ivPhoto);
    }


    public class PhotoViewHolder extends RecyclerView.ViewHolder {
        ImageView ivPhoto;

        public PhotoViewHolder(@NonNull View itemView) {
            super(itemView);
            ivPhoto = itemView.findViewById(R.id.imageView);
        }
    }
}
