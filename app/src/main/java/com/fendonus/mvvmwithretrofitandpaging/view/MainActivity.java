package com.fendonus.mvvmwithretrofitandpaging.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.paging.PagedList;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.fendonus.mvvmwithretrofitandpaging.R;
import com.fendonus.mvvmwithretrofitandpaging.adapter.PhotosAdapter;
import com.fendonus.mvvmwithretrofitandpaging.model.Photos;
import com.fendonus.mvvmwithretrofitandpaging.viewmodel.MainActivityViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    MainActivityViewModel mainActivityViewModel;
    @BindView(R.id.recylerview)
    RecyclerView photoRecylerview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mainActivityViewModel = ViewModelProviders.of(this).get(MainActivityViewModel.class);

        photoRecylerview.setLayoutManager(new GridLayoutManager(this,3));

        mainActivityViewModel.getPagedListLiveData().observe(this, new Observer<PagedList<Photos>>() {
            @Override
            public void onChanged(@Nullable PagedList<Photos> photos) {
                PhotosAdapter photosAdapter = new PhotosAdapter();
                photosAdapter.submitList(photos);
                photoRecylerview.setAdapter(photosAdapter);
            }
        });
    }
}
