package com.fendonus.mvvmwithretrofitandpaging.network;

import com.fendonus.mvvmwithretrofitandpaging.model.Photos;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetDataService {

    @GET("/photos")
    Call<List<Photos>> getAllPhotos(@Query("albumId") long albumId);
}